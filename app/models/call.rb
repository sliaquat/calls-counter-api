class Call < ApplicationRecord
  default_scope { order(created_at: :asc) }
  serialize :payload
end
