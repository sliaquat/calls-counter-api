module API
  class CallsController < ApplicationController
    def index
      @calls = Call.all
      json_response(@calls)
    end

    def last
      @call = Call.last
      json_response(@call)
    end

    def create
      body = request.body.read
      payload = body.blank? ? nil : JSON.parse(body)
      @call = Call.create!(payload: payload)
      json_response(@call, :created)
    end

    def count
      @count = Call.count
      json_response({ count: @count })
    end

    def reset
      Call.delete_all
      json_response({ message: "Reset done" })
    end
  end
end
