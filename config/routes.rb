Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    resources :calls, only: [:create, :index]

    get 'calls/count', to: 'calls#count'

    get 'calls/last', to: 'calls#last'

    post 'calls/reset', to: 'calls#reset'
  end
end
