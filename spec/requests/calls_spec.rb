require 'rails_helper'

RSpec.describe 'Calls API', type: :request do
  describe 'POST /calls' do
    before { post '/api/calls' }
    it 'returns the created call' do
      expect(json).not_to be_empty
    end

    it 'returns status code 201' do
      expect(response).to have_http_status(201)
    end
  end

  describe 'GET /calls/last' do
    let(:payload) { { "author_id": 1, "description": "This is a description" } }

    before do
      post '/api/calls', params: payload.to_json
      get '/api/calls/last'

    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns the payload' do
      expect(json["payload"]).to eq payload.stringify_keys
    end
  end

  describe 'GET /calls' do
    before do
      post '/api/calls'
      post '/api/calls'
      get '/api/calls'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns all calls' do
      expect(json.size).to eq(2)
    end
  end

  describe 'GET /calls/count' do
    before do
      post '/api/calls'
      get '/api/calls/count'
    end

    it 'returns the current count' do
      expect(json['count']).to eq(1)
    end

    it 'increment counts on successive calls' do
      post '/api/calls'
      get '/api/calls/count'
      expect(json['count']).to eq(2)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'POST /calls/reset' do
    before do
      post '/api/calls'
      post '/api/calls/reset'
      get '/api/calls/count'
    end

    it 'returns the current count' do
      expect(json['count']).to eq(0)
    end
  end
end
