# README

This is a Rails api application that provides endpoints for testing webhooks.  

The following endpoints are available:

* POST /api/calls (Stores the payload)
* GET /api/calls
* GET /api/calls/last
* GET /api/calls/count
* POST /api/calls/reset

## Running the application

### On local machine

You will need to install ruby.

1. Install bundler:
`gem install bundler`

1. Install the Gems:
`bundle install`

1. Initialize the db:
`rails db:create`

1. Run the application:
`rails server` 

The application will be accessible at http://localhost:3000

### Inside a docker container

You will need to have docker installed and running on your machine

1. Build the docker image:
`docker build -t calls-counter-api .`

1. Run the image in a docker container:
`docker run --name calls-counter-api --hostname localhost -p 3000:3000 calls-counter-api:latest`

The application will be accessible at http://localhost:3000

## Running the tests

Run the rspec tests by executing:

`bundle exec rspec`
