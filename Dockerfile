FROM ruby:2.6.3

MAINTAINER Sanad H Liaquat <sanadhussain@gmail.com>

ENV APP_HOME /myapp

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/

ENV RAILS_ENV production

RUN bundle install

ADD . $APP_HOME

RUN rails db:reset

EXPOSE 3000

CMD ["puma"]
